# -*- coding: utf-8 -*-
from unittest import TestCase
from parameterized import parameterized
from fizzbuzz import FizzBuzz


class FizzBuzzTest(TestCase):

    def setUp(self):
        self.fizzbuzz = FizzBuzz()

    def tearDown(self):
        self.fizzbuzz = None

    @parameterized.expand([
        (1, '1'), (2, '2'), (4, '4'), (7, '7'),
        (11, '11'), (13, '13'), (14, '14'),
        ])
    def test_returns_number_for_input_not_divisible_by_3_or_5(self,
                                                              value, expected):
        self.assertEqual(expected, self.fizzbuzz.convert(value))

    @parameterized.expand([
        (3, 'Fizz'), (6, 'Fizz'), (9, 'Fizz'),
        (12, 'Fizz'), (18, 'Fizz'),
        (21, 'Fizz'), (24, 'Fizz'), (27, 'Fizz'),
        ])
    def test_returns_fizz_for_input_divisible_by_3_but_not_5(self,
                                                             value, expected):
        self.assertEqual(expected, self.fizzbuzz.convert(value))

    @parameterized.expand([
        (5, 'Buzz'),
        (10, 'Buzz'),
        (20, 'Buzz'), (25, 'Buzz'),
        (35, 'Buzz'),
        (40, 'Buzz'),
        (50, 'Buzz'),
        ])
    def test_returns_buzz_for_input_divisible_by_5_but_not_3(self,
                                                             value, expected):
        self.assertEqual(expected, self.fizzbuzz.convert(value))

    @parameterized.expand([
        (15, 'FizzBuzz'), (30, 'FizzBuzz'), (45, 'FizzBuzz'),
        (60, 'FizzBuzz'), (75, 'FizzBuzz'), (90, 'FizzBuzz'),
        (105, 'FizzBuzz'),
        ])
    def test_returns_fizzbuzz_for_input_divisible_by_3_and_5(self,
                                                             value, expected):
        self.assertEqual(expected, self.fizzbuzz.convert(value))
